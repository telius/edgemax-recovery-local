#!/bin/sh
#
# emrk-remove-user-data: removes and re-creates writable directory
#
# Maintainer: Adin Kapul < telius at cock dot li >
#
# Copyright (C) 2013 SO3Group, Adin Kapul
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#

select_usb() {
    while [ ! -b /dev/$DEV ] ;do
        USB_DEVICES=($(
                xargs -n1 readlink < <(echo /sys/block/*) |
                sed -ne 's+^.*/usb[0-9].*/\([^/]*\)$+/sys/block/\1/device/uevent+p' |
                xargs grep -H ^DRIVER=sd |
                sed s/device.uevent.*$/size/ |
                xargs grep -Hv ^0$ |
                cut -d / -f 4                 ))
        if [ ${#USB_DEVICES[@]} -eq 0 ];then
            title="No flash drives were found"
        else
            title="Please select your EdgeRouter\'s flash drive"
        fi

        menu=(R "Re scan devices")
        for dev in ${USB_DEVICES[@]} ;do
            read model </sys/block/$dev/device/model
            size=$(expr $(blockdev --getsize64 /dev/$dev) / 1048576)
	    menu+=($dev "$model ($size MiB)")
        done
        num=$(whiptail --menu "$title" 21 72 14 "${menu[@]}" 2>&1 >/dev/tty)
        if [ ! "$num" ] ; then
            echo "User aborted."
            exit 0;
        fi
        [ ! "$num" = "R" ] && [ "${USB_DEVICES[num]}" ] && DEV=${USB_DEVICES[num]}
    done
    DEV="/dev/$DEV"
    BOOT=${DEV}1
    ROOT=${DEV}2
}

YESNO=$(dirname $0)/yesno
SHRED="shred -fu"
ROOT_MNT_DIR=/tmp/edgemax-recovery/root

echo "WARNING: this script will purge all the data"
echo "in user-writable directories!"
echo "All your configuration, user installed packages"
echo "and anything else you had apart from the default"
echo "EdgeOS files will be irrecoverably destroyed"
echo "Do you want to continue?"

$YESNO

if [ $? == 1 ]; then
    exit 0
fi

# identify ER's flash drive
read -p "Please select your EdgeRouter's flash drive"
select_usb

umount $ROOT
mount -t ext3 $ROOT $ROOT_MNT_DIR
# Why w* ?
# Because when you press the reset button, your old data
# is not removed. The old w/ dir is renamed to w${random string}/
# and the new one is created
# In most cases it's ok, but if you are going e.g. to sell
# the router on ebay or otherwise give it away to untrusted
# third party, transfer of config date and encryption key
# is a very bad idea
find $ROOT_MNT_DIR -name w\* -type f -exec $SHRED {} \;
find $ROOT_MNT_DIR -name w\* -print  | xargs /bin/rm -rf

mkdir $ROOT_MNT_DIR/w
umount $ROOT

echo "User data has been shredded.
